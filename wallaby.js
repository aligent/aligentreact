module.exports = function (wallaby) {
    let path = require('path');

    process.env.NODE_PATH +=
      path.delimiter +
      path.join(__dirname, 'node_modules') +
      path.delimiter +
      path.join(__dirname, 'node_modules/react-scripts/node_modules');

    require('module').Module._initPaths();
    const babelRc = JSON.parse(require('fs').readFileSync(`${process.cwd()}/.babelrc`));
    return {

        files: [
            'src/lib/**/*.+(js|jsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
            '!src/lib/**/*.test.js'
        ],

        tests: [
            'src/lib/**/*.test.js?(x)'
        ],

        env: {
            type: 'node',
            runner: 'node'
        },

        compilers: {
            'src/lib/**/*.js?(x)': wallaby.compilers.babel({
                babel: require('babel-core'),
                presets: babelRc.presets,
                plugins: babelRc.plugins
            })
        },

        preprocessors: {
            'src/lib/**/*.js?(x)': file => require('babel-core').transform(
                file.content,
                {
                    sourceMap: true,
                    filename: file.path
                }
)
        },

        testFramework: 'jest'
    };
};
