# Aligent Checkout

A set of React components that can be used to create an E-commerce checkout.

## Getting Started

### Usage

```bash
npm install --save git+ssh//git@bitbucket.org:aligent/aligentcheckout.git
```

Import components into your local React app/components as required

## Development

Clone repository
```bash
git clone git@bitbucket.org:aligent/aligentcheckout.git AligentCheckout
```

Install dependencies
```bash
npm install
```

Run local development server
```bash
npm run dev
```

Follow these steps to being developing a new component

1. Create a new branch at the same location as `master`
2. Create new component file: `src/lib/components/[ComponentName]/[ComponentName].jsx`
3. Import and export the new component in `src/lib/index.js` (follow format of existing components - try to keep the list in alphabetical order)
4. Open `src/docs/App.jsx`, import the new component, and place it inside `<React.Fragment>` so that it will be rendered
5. Create associated test file: `src/lib/components/[ComponentName]/[ComponentName].test.js`
6. Start developing!

### Component vs Container

Every `component` should have an associated `container`. The `container` is what will be imported into various projects, and will contain any logic required to determine the values that the `component` should display.

### Stateless Functional Components / Pure Components

Each `component` should be written as a Stateless Functional Component (SFC), also known as a pure component. A pure component is such that with the same input, the output will always be the same. Both in presentation, and in app state.

*The following example is very simplistic, and not really representative of a real world situation, but it does show  how to re-write component as an SFC/Pure Component*

**Before**

```javascript
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class HelloWorld extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>Hello {this.props.name}</div>
    );
  }
}

HelloWorld.propTypes = {
  name: PropTypes.string.isRequired
}

export default HelloWorld;
```

**After**
```javascript
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class HelloWorld = ({ name }) => {
  return (
    <div>Hello {name}</div>
  );
}

HelloWorld.propTypes = {
  name: PropTypes.string.isRequired
}

export default HelloWorld;
```

### Testing

Each component should have an accompanying test file. This file will be place in the same directory, with a name that is very similar to the component. For example, `MyComponent.jsx` will have a test file `MyComponent.test.js`.

To run the tests, simply call `npm test`

The testing framework is Jest, which means there is little to no config set up required. Simply putting `.test` in the filename will ensure the file will be tested.

**At the very minimum, a test must be included that ensures the component can render without any errors**

### Using development JSON API

The package [JSON Server](https://github.com/typicode/json-server) has been configured in the project to allow for API requests to be made to a location that can easily have some test data set up. In order to use the json server:

1. Update `db.json` with a new root parameter. The root parameter will line up with the first parameter in the URL query string.
Refer to existing "stores" data property

2. Run the API server

```bash
npm run fakeapi
```

3. Make calls to the server:

```javascript
fetch('http://localhost:3000/{root_parameter}')
    .then(response => response.json())
    .then((data) => {
         console.log(data)
    });
```

Ideally, that data would be set into state, so your component will automatically re-render with the data

### Creating a Pull Request

1. Make sure code is linted
2. Make sure tests pass
3. Run `npm run transpile` to build the output file
4. Create PR on Bitbucket into the `develop` branch, and assign to 2 devs
5. Once approved by both devs, PR can be merged
